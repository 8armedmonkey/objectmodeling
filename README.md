# Object Modeling

* Use model to solve complexity.
* Good model will simplify a lot of things.
* Objects are not merely data structures (data container with
  getters & setters).
* Some objects are valid data structures (e.g.: Color). Some are not
  (e.g: Person), they have identity & behaviors.
* Interactions between objects have to be meaningful.
* Modeling constraints in the object helps ensuring the integrity of
  that object (i.e.: prevent unwanted modification / state corruption).
* Data structure / value object should be immutable.
* Continuously refactor to improve the model / design.

### This is Bad
![bad](doc/img/bad.jpg)

### This is Good
![good](doc/img/good.jpg)