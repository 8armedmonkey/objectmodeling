package example.object.modeling;

import example.object.modeling.better.Food;
import example.object.modeling.better.Person;
import example.object.modeling.better.PersonFactory;

public class DemoBetterModel {

    public static void main(String[] args) {
        Person person = PersonFactory.newPerson();
        person.talk();
        person.receive(new Food("Banana"));
    }

}
