package example.object.modeling.better;

abstract class Action implements Runnable {

    private final Runnable onStart;
    private final Runnable onComplete;

    Action(Runnable onStart, Runnable onComplete) {
        this.onStart = onStart;
        this.onComplete = onComplete;
    }

    @Override
    public void run() {
        onStart.run();
        perform();
        onComplete.run();
    }

    abstract void perform();

}
