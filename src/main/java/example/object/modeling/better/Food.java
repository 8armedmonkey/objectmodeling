package example.object.modeling.better;

public class Food {

    private final String name;

    public Food(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

}
