package example.object.modeling.better;

class Mouth {

    private Activity activity;
    private OnIdleListener onIdleListener;

    Mouth() {
        activity = Activity.IDLE;
    }

    void speak(String sentence) {
        synchronized (this) {
            if (isIdle()) {
                new Thread(
                    new Speaking(sentence, this::setSpeaking, this::setIdle)
                ).start();
            } else {
                throw new IllegalStateException("Mouth is currently not idle.");
            }
        }
    }

    void chew(Food food) {
        synchronized (this) {
            if (isIdle()) {
                new Thread(
                    new Chewing(food, this::setChewing, this::setIdle)
                ).start();
            } else {
                throw new IllegalStateException("Mouth is currently not idle.");
            }
        }
    }

    boolean isIdle() {
        synchronized (this) {
            return Activity.IDLE == activity;
        }
    }

    void onIdle(OnIdleListener onIdleListener) {
        this.onIdleListener = onIdleListener;
    }

    private void setIdle() {
        synchronized (this) {
            activity = Activity.IDLE;

            if (onIdleListener != null) {
                onIdleListener.onIdle(this);
                onIdleListener = null;
            }
        }
    }

    private void setSpeaking() {
        synchronized (this) {
            activity = Activity.SPEAKING;
        }
    }

    private void setChewing() {
        synchronized (this) {
            activity = Activity.CHEWING;
        }
    }

    private enum Activity {
        IDLE, SPEAKING, CHEWING
    }

    interface OnIdleListener {
        void onIdle(Mouth mouth);
    }

}
