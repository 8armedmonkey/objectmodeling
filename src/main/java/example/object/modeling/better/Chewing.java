package example.object.modeling.better;

import example.object.modeling.ThreadUtils;

class Chewing extends Action {

    private static final long DELAY_MILLIS = 300;

    private final Food food;

    Chewing(Food food, Runnable onStart, Runnable onComplete) {
        super(onStart, onComplete);
        this.food = food;
    }

    @Override
    void perform() {
        System.out.println("Chewing...");

        for (char c : food.getName().toCharArray()) {
            System.out.print(c);
            ThreadUtils.sleep(DELAY_MILLIS);
        }

        System.out.println("\nDone chewing.");
    }

}
