package example.object.modeling.better;

public final class PersonFactory {

    private PersonFactory() {
    }

    public static Person newPerson() {
        return new Person(new Mouth(), new Hand());
    }

}
