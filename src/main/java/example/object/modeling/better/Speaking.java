package example.object.modeling.better;

import example.object.modeling.ThreadUtils;

class Speaking extends Action {

    private static final long DELAY_BETWEEN_CHARS_MILLIS = 100;

    private final String sentence;

    Speaking(String sentence, Runnable onStart, Runnable onComplete) {
        super(onStart, onComplete);
        this.sentence = sentence;
    }

    @Override
    public void perform() {
        System.out.println("Speaking...");

        for (char c : sentence.toCharArray()) {
            System.out.print(String.format("[ %c ]", c));
            ThreadUtils.sleep(DELAY_BETWEEN_CHARS_MILLIS);
        }

        System.out.println("\nDone speaking.");
    }

}
