package example.object.modeling.better;

public class Person {

    private final Mouth mouth;
    private final Hand hand;

    Person(Mouth mouth, Hand hand) {
        this.mouth = mouth;
        this.hand = hand;
    }

    public void receive(Food food) {
        hand.hold(food);

        if (mouth.isIdle()) {
            hand.putFoodInMouth(mouth);
        } else {
            mouth.onIdle(hand::putFoodInMouth);
        }
    }

    public void talk() {
        if (mouth.isIdle()) {
            mouth.speak(Sentences.pickRandomOne());
        }
    }

}
