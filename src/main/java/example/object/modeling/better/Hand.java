package example.object.modeling.better;

class Hand {

    private Food food;

    void hold(Food food) {
        synchronized (this) {
            System.out.println("Hold the food.");
            this.food = food;
        }
    }

    void putFoodInMouth(Mouth mouth) {
        synchronized (this) {
            if (isHoldingFood()) {
                System.out.println("Put the food in mouth.");
                mouth.chew(food);
                food = null;
            } else {
                throw new IllegalStateException("Hand is not holding food.");
            }
        }
    }

    private boolean isHoldingFood() {
        synchronized (this) {
            return food != null;
        }
    }

}
