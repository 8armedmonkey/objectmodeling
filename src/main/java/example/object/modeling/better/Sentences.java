package example.object.modeling.better;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

final class Sentences {

    private static final List<String> entries = new ArrayList<>();
    private static final Random random = new Random();

    static {
        entries.add("Hello there.");
        entries.add("How are you?");
        entries.add("The weather is nice.");
        entries.add("Nice to meet you.");
    }

    static String pickRandomOne() {
        return entries.get(random.nextInt(entries.size()));
    }

}
