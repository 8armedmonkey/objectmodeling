package example.object.modeling;

public final class ThreadUtils {

    private ThreadUtils() {
    }

    public static void sleep(long millis) {
        try {
            Thread.sleep(millis);
        } catch (InterruptedException e) {
            // Ignore error.
        }
    }

}
