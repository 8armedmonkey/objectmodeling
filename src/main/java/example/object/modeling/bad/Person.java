package example.object.modeling.bad;

public class Person {

    private Mouth mouth;

    public Mouth getMouth() {
        return mouth;
    }

    public void setMouth(Mouth mouth) {
        this.mouth = mouth;
    }

}
