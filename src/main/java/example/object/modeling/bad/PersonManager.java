package example.object.modeling.bad;

public class PersonManager {

    private Person person;

    public Person getPerson() {
        return person;
    }

    public void setPerson(Person person) {
        this.person = person;
    }

    public void makeTalk(String sentence) {
        if (person != null && person.getMouth() != null) {
            new Thread(
                new SpeakingTask(person.getMouth(), sentence)
            ).start();
        }
    }

    public void makeChew(Food food) {
        if (person != null && person.getMouth() != null) {
            new Thread(
                new ChewingTask(person.getMouth(), food)
            ).start();
        }
    }

}
