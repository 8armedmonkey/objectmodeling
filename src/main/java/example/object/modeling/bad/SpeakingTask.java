package example.object.modeling.bad;

import example.object.modeling.ThreadUtils;

public class SpeakingTask implements Runnable {

    private static final long DELAY_BETWEEN_CHARS_MILLIS = 100;

    private Mouth mouth;
    private String sentence;

    public SpeakingTask(Mouth mouth, String sentence) {
        this.mouth = mouth;
        this.sentence = sentence;
    }

    @Override
    public void run() {
        System.out.println("Speaking...");

        mouth.setSentence(sentence);

        for (char c : mouth.getSentence().toCharArray()) {
            System.out.print(String.format("[ %c ]", c));
            ThreadUtils.sleep(DELAY_BETWEEN_CHARS_MILLIS);
        }

        System.out.println("\nDone speaking.");
    }

}
