package example.object.modeling.bad;

import example.object.modeling.ThreadUtils;

public class ChewingTask implements Runnable {

    private static final long DELAY_MILLIS = 300;

    private Mouth mouth;
    private Food food;

    public ChewingTask(Mouth mouth, Food food) {
        this.mouth = mouth;
        this.food = food;
    }

    @Override
    public void run() {
        System.out.println("Chewing...");

        mouth.setFood(food);

        for (char c : mouth.getFood().getName().toCharArray()) {
            System.out.print(c);
            ThreadUtils.sleep(DELAY_MILLIS);
        }

        System.out.println("\nDone chewing.");
    }

}
