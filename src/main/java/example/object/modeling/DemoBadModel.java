package example.object.modeling;

import example.object.modeling.bad.*;

public class DemoBadModel {

    public static void main(String[] args) {
        Person person = new Person();
        person.setMouth(new Mouth());

        PersonManager personManager = new PersonManager();
        personManager.setPerson(person);

        String sentence = "Damn you!";
        personManager.makeTalk(sentence);

        Food food = new Food();
        food.setName("Banana");
        personManager.makeChew(food);
    }

}
